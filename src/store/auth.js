export const state = () => ({
  token: '',
  status: '',
  user: ''
});

export const getters = {
  isAuthenticated: (state) => !!state.token
};

export const mutations = {
  authRequest(state) {
    state.status = 'loading';
  },

  authSuccess(state, token) {
    state.status = 'success';
    if (process.browser) localStorage.setItem('token', token);
    state.token = token;
  },

  setUser(state, user) {
    state.user = user;
  },

  authError(state, errorCode) {
    if (process.browser) {
      if (localStorage.getItem('token')) localStorage.removeItem('token');
    }
    state.token = '';
    state.user = '';
    state.status = errorCode;
  },

  setTokenOnStartup(state, token) {
    state.status = 'success';
    state.token = token;
  },

  removeToken(state) {
    if (process.browser) {
      if (localStorage.getItem('token')) localStorage.removeItem('token');
    }
    state.token = '';
  }
};

export const actions = {
  login(context, user) {
    return new Promise((resolve, reject) => {
      context.commit('authRequest');
      this.$axios
        .post(`/auth/login`, user, {
          headers: {
            'content-type': 'application/json'
          }
        })
        .then((res) => {
          const token = res.data.token.substring(7);
          context.commit('authSuccess', token);
          const user = res.data.payload;
          context.commit('setUser', user);
          console.log(context);
          resolve(res);
        })
        .catch((err) => {
          context.commit('authError', err.response.status);
          reject(err);
        });
    });
  },

  register(context, user) {
    return new Promise((resolve, reject) => {
      context.commit('authRequest');
      this.$axios
        .post(`/users`, user, {
          headers: {
            'content-type': 'application/json'
          }
        })
        .then((res) => {
          console.log(res);
          context.dispatch('login', user);
          resolve(res);
        })
        .catch((err) => {
          console.log(err);
          context.commit('authError', err.response.status);
          reject(err);
        });
    });
  },

  logout(context) {
    return new Promise((resolve, reject) => {
      context.commit('authRequest');
      this.$axios
        .post(`/users/logout`)
        .then((res) => {
          context.commit('removeToken');
          resolve(res);
        })
        .catch((err) => {
          context.commit('authError', err.response.status);
          reject(err);
        });
    });
  }
};
