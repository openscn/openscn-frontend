import axios from 'axios';
require('dotenv').config();

export default async function ({ store, redirect }) {
  if (store.getters['auth/isAuthenticated']) {
    console.log('Verifying user');
    try {
      const validationResponse = await axios.get(
        `${process.env.API_URL}/auth/user-verify`,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${store.state.auth.token}`
          }
        }
      );
      store.commit('auth/setUser', validationResponse.data.payload);
    } catch (error) {
      store.commit('auth/authError', 401);
    }
  }
}
