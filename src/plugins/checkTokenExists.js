export default function ({ store }) {
  if (process.browser) {
    const token = localStorage.getItem('token');
    if (token) {
      store.commit('auth/setTokenOnStartup', token);
    }
  }
}
