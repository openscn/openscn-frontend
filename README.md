# openscn-frontend

> Front end Vue application for OpenSCN

## Build Setup

Copy the `.env.example` to `.env` and replace the value of `OPENSCN_WEB_API_URL`
to the url of the API you are using.

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
