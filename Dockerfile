FROM node:12.16.1-alpine AS development

RUN apk upgrade
RUN apk add --update bash
RUN apk add --update alpine-sdk
RUN apk add g++ make python
RUN mkdir /app
WORKDIR /app
COPY . /app
ENV NODE_ENV=development
RUN npm install
RUN npm run build


FROM node:12.16.1-alpine AS production

COPY --from=development /app /app
WORKDIR /app
ENV NODE_ENV=production
CMD npm start -- --port 80
